package qalab;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
//import org.apache.uima.examples.SourceDocumentInformation;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

public class AE extends JCasAnnotator_ImplBase {

	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		// TODO Auto-generated method stub
		super.initialize(aContext);
	}
	
	@Override
	public void process(JCas cas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub
		MyType annotation = new MyType(cas);
		annotation.setBegin(0);
		annotation.setEnd(2);
		annotation.addToIndexes();
	}

}
