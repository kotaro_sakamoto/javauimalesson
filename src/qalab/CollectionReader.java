package qalab;

import java.io.Closeable;
import java.io.IOException;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;

public class CollectionReader extends CollectionReader_ImplBase implements Closeable {

	@Override
	public void initialize() throws ResourceInitializationException {
		// TODO Auto-generated method stub
		super.initialize();
	}
	
	@Override
	public void getNext(CAS arg0) throws IOException, CollectionException {
		// TODO Auto-generated method stub
		try {
			JCas jcas = arg0.getJCas();
			jcas.setDocumentText("サンプルテキストです。");
		} catch (CASException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Progress[] getProgress() {
		// TODO Auto-generated method stub
		return null;
	}

	private boolean released = false;
	
	@Override
	public boolean hasNext() throws IOException, CollectionException {
		// TODO Auto-generated method stub
		if (released) {
			return false;
		} else {
			released = true;
			return true;
		}
	}

}
